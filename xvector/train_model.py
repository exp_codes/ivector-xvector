import pickle
import time

import numpy
import numpy as np
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
import torch.utils.data
from python_speech_features import mfcc
from scipy.io import wavfile
from torch import autograd
from tqdm import tqdm

from xvector.tdnn import FullyConnected
from xvector.tdnn import StatsPooling
from xvector.tdnn import TDNN
print('GPU cuda is okay?',torch.cuda.is_available())

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def to_categorical(y, num_classes):
    """ 1-hot encodes a tensor """
    return np.eye(num_classes, dtype='uint8')[y]



clases = ['Trump','Macron','Gaga','Clinton']

#%%

# X = []
# y = []
# max_total_context = 400
# _min,_max = float('inf'),-float('inf')

# for wav_name in os.listdir('train_cleaned'):
#     if wav_name.endswith('wav'):
#         print (wav_name)
#         rate, wav = wavfile.read(r'train_cleaned/'+wav_name)
#         for chunked_wav in tqdm(chunks(wav,int(len(wav)/4))):
#             X_sample = mfcc(chunked_wav,samplerate= rate,numcep=24
#                    ,nfilt=26,nfft=1024)
#             _min = min(np.amin(X_sample),_min)
#             _max = max(np.amax(X_sample),_max)

#             for chunked_X_sample in list(chunks(X_sample,max_total_context)):
#                 if len(chunked_X_sample) == max_total_context:
#                     X.append(chunked_X_sample)
#                     y.append(to_categorical(clases.index(wav_name.split('.wav')[0]),len(clases)))
#                 else:
#                     print ('discarded box')
# X = (X - _min) / (_max-_min)
# with open('X_data.pkl','wb') as f:
#     pickle.dump((X,y),f)

#%%

with open('X_data.pkl','rb') as f:
    X,y =  pickle.load(f)

#%%

# gg = [0,0,0]*8
#in the input LINES we keep the MFCC's values
#in the input ROWS we keep the diffrent frames
# zz = [1.0,2.0,3.0]*8
# a= [gg,gg,zz,zz,zz,zz,zz,zz,zz]*30
# train_data = []
# x_data = np.array([a]).astype(np.double)
# labels = ['a','b','c','d','e']
# for i in range(len(x_data)):
#     train_data.append([x_data[i], labels[i]])

trainloader = torch.utils.data.DataLoader(list(zip(X,y)), shuffle=True, batch_size=5,drop_last=True)


net1 = TDNN(input_dim=24, output_dim=512, context_size=5, dilation=1)
net2 = TDNN(input_dim=512, output_dim=512, context_size=3, dilation=2)
net3 = TDNN(input_dim=512, output_dim=512, context_size=3, dilation=3)
net4 = TDNN(input_dim=512, output_dim=512, context_size=1, dilation=1)
net5 = TDNN(input_dim=512, output_dim=1500, context_size=1, dilation=1)
SP = StatsPooling()
FC = FullyConnected()
Final = nn.Linear(512,4).double()
net = nn.Sequential(net1,net2,net3,net4,net5,SP,FC,Final)


#%%

optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)
criterion = nn.CrossEntropyLoss(reduction='mean')


epocs = 50
count_iter=0
with autograd.detect_anomaly():
    for j in range (epocs):
        for i, data in enumerate(trainloader, 0):

            count_iter+=1

            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = data
            # print(numpy.any(numpy.isnan(inputs.to('cpu'))))
            # print(numpy.any(numpy.isnan(labels.to('cpu'))))
            inputs, labels = inputs.to('cuda:0'), labels.to('cuda:0')

            net=net.double()
            net.to('cuda:0')
            output= net(inputs).to('cuda:0')
            optimizer.zero_grad()
            loss = criterion(output, torch.max(labels, 1)[1])
            loss.backward()
            net.float()
            optimizer.step()

            print ("step: {} loss: {} ".format(count_iter,loss))
            # print ("output:\n{} \n labels:\n{}".format(output,labels))
            torch.save(net, 'total_model')


#%% md

### Embedding

#%%

net_xvec = nn.Sequential(net1,net2,net3,net4,net5,SP,FC)

#%%



#%% md

### Testing

#%%

net = torch.load('total_model')
net.eval()

#%%

for p in list(net.parameters()):
    print (p.shape)

#%%

list(net[0].parameters())

#%%

for p in list(net.parameters()):
    print (p)

#%%



#%%

wav_path = "test.wav"
max_total_context_test = 400
_min,_max = float('inf'),-float('inf')
X_test = []
rate, wav = wavfile.read(wav_path)
for chunked_wav in tqdm(chunks(wav,int(len(wav)/4))):
    X_sample = mfcc(chunked_wav,samplerate= rate,numcep=24
                    ,nfilt=26,nfft=1024)
    _min = min(np.amin(X_sample),_min)
    _max = max(np.amax(X_sample),_max)
    for chunked_X_sample in list(chunks(X_sample,max_total_context_test)):
        if len(chunked_X_sample) == max_total_context_test:
            X_test.append(chunked_X_sample)
X_test = (X_test - _min) / (_max-_min)


#%%

print(torch.tensor(X_test).shape)
#
# #%%
#
# del net
#
# #%%
#
# net.to('cpu')
#
# #%%
#
# output= net(torch.tensor(X_test[100:120]).cuda())
# for out in output:
#     print (clases[torch.argmax(out).cuda()])
#
# #%%
#
# print(X_test[:70].shape)
#
# #%%
#
# torch.cuda.empty_cache()
#
# #%%
#
# torch.cuda.max_memory_allocated()
#
# #%%
#
# torch.cuda.reset_max_memory_allocated()
#
# #%%
#
# torch.cuda.reset_max_memory_cached()
#
# #%%
#
# del X_test
#
# #%%
#
# print(X_test.shape)
#
# #%%
#
# 150 * 44100
#
# #%%
#
# print(X_test.shape)
#
# #%%
#
# torch.cuda.empty_cache()
#
# #%%
#
# torch.argmax(output[0])
#
# #%%
#
# torch.max(labels, 1)
#
# #%%
#
# print(labels[0])
#
# #%%
#
# output
#
# #%%
#
# loss
#
# #%%
#
# loss = criterion(output, torch.max(labels, 1)[1])
#
#
# #%% md
#
# #loss: 1.394379672011826 step took 183.8471429347992
#
# #%%
#
# torch.tensor([X[0]]).shape
#
# #%%
#
# for i, data in enumerate(trainloader, 0):
#     # get the inputs; data is a list of [inputs, labels]
#     inputs, labels = data
#
# #%%
#
# len(np.array([X[0],X[1]]).shape)
#
# #%%
#
# output= net(inputs)#.to(device)
#
#
# #%%
#
# net1(inputs).shape
#
# #%%
#
# for i, data in enumerate(trainloader, 0):
#     a = time.time()
#     # get the inputs; data is a list of [inputs, labels]
#     inputs, labels = data
#     break
#
# #%%
#
# net1(inputs.cuda()).shape
#
# #%%
#
# mean = net1(inputs.cuda()).mean(dim=1)
#
# #%%
#
# mean.shape
#
# #%%
#
# inputs.shape
#
# #%%
#
# output.reshape((2,1,4))
#
# #%%
#
# (torch.max(labels, 1)[1]).shape
#
# #%%
#
# output.shape
#
# #%%
#
# out
#
# #%%
#
# criterion(torch.tensor([]))
#
# #%%
#
# loss = criterion(output, torch.max(labels, 1)[1])
# loss.backward()
# optimizer.step()
#
# #%%
#
# torch.tensor([[0,0,0,999],[0,0,0,999]])
#
# #%%
#
# mean = output.mean(dim=1)
# std = output.std(dim=1)
# # ms = torch.cat((mean,std),dim=1)
#
# #%%
#
# nn.
#
# #%%
#
# (mean*396).shape
#
# #%%
#
# mean[2].shape
#
# #%%
#
# output[::1,].shape
#
# #%%
#
# output[:,:1,] - mean[1]
#
# #%%
#
# output.std(dim=1).shape
#
# #%%
#
# Final(FC(ms)).shape
#
# #%%
#
# SP(output).shape
#
# #%%
#
# inputs.shape
#
# #%%
#
# net.load_state_dict
#
# #%%
#
# inputs
#
# #%%
#
# torch._C._cuda_init()
#
# #%%
#
# output.reshape((1,4))
#
# #%%
#
# criterion = nn.CrossEntropyLoss()
# criterion(output.reshape((1,4)), torch.max(labels, 1)[1])
#
# #%%
#
# output = net1(inputs.cuda())
#
# #%%
#
# F.normalize(output,dim=1).shape
#
# #%%
#
# o=torch.tensor([[[0,2,4],[4,5,6],[7,8,9]],[[1,2,3],[4,5,6],[7,8,9]],[[1,2,3],[4,5,6],[7,8,9]]]).float()
#
# #%%
#
# mean = output.mean(dim=2)
# std = output.std(dim=2)
# print (mean)
# print ()
# print (std)
#
# #%%
#
# mean.shape
#
# #%%
#
# ((output-mean.transpose(0,1)))#.transpose(0,2) /  std.transpose(0,1)).transpose(0,2)
#
# #%%
#
# (o-mean.transpose(0,1)).transpose(1,2) /  std.transpose(0,1)
#
# #%%
#
# F.normalize(o.float(),dim=2)
#
# #%%
#
#
#
# #%%
#
# o
#
# #%%
#
# torch.tensor([0,0,1,0]).reshape((1,4))
#
# #%%
#
# labels
#
# #%%
#
# criterion(torch.tensor([0,0,1,0]).reshape((1,4)).float(), torch.max(labels, 1)[1])
#
# #%%
#
# torch.cat( (output.mean(dim=1)[0],output.std(dim=1)[0] )).shape
#
# #%%
#
# output
#
# #%%
#
# F.softmax(output).shape
#
# #%%
#
# L2 = nn.Linear(512,512).double()
#
# #%%
#
# L2(F.relu(output))
#
# #%%
#
# output.mean(dim=1)
#
# #%%
#
# output[0].std(dim=0)
#
# #%%
#
# output[0][0]
#
# #%%
#
# output[0][1]
#
# #%%
#
# output
#
# #%%
#
# output[0][2]
#
# #%%
#
# output[0][3]
