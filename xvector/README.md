﻿# Pytorch implementation of X-vectors embedding

This repository is using https://github.com/SiddGururani/Pytorch-TDNN]

# Files

TDNN_gpu.py
TDNN layers implementation, Stats pooling and final layers implementation

total_model
xvector - gpu.ipynb
Jupyter notebook for the network training

xvector_ext_and_lda_scatter.ipynb
Notebook that extracts X-vectors on new utterances and via LDA reduces thier dimensions and plots them in 2D and 3D
