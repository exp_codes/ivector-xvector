from ivector.data_init import Initializer
from ivector.extract_features import FeaturesExtractor
from ivector.i_vector import IVector
conf_filename = "conf.yaml"
init = Initializer(conf_filename)
init.structure()

ex = FeaturesExtractor(conf_filename)
ex.extract_features("enroll")
ex.extract_features("test")


iv = IVector(conf_filename)
iv.train_tv()
iv.evaluate()
print( "Accuracy: {}%".format(iv.getAccuracy()) )