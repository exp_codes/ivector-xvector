import os
from pydub import AudioSegment
def song_in_singer_name_folder(artist_dir):
    for root , dirs, names in os.walk(artist_dir):
        for name in names:
            file_path=os.path.join(root,name)
            if '.mp3' in file_path:
                print(file_path)
                file_path_list=file_path.split(os.sep)
                des_path='../../20artist/'+file_path_list[1]+'/'+name
                print(des_path)
                des_dir=os.path.dirname(des_path)
                if not os.path.isdir(des_dir):
                    os.makedirs(des_dir)
                os.rename(file_path,des_path)
    return 0

def convert_mp3_wav(artist_dir):
    for root , dirs, names in os.walk(artist_dir):
        for name in names:
            file_path=os.path.join(root,name)
            if '.mp3' in file_path:
                print(file_path)
                song=AudioSegment.from_mp3(file_path)
                song=song[30000:60000]
                file_path_list=file_path.split(os.sep)
                des_path='../../artist20/'+file_path_list[1]+'/'+name.replace('.mp3','.wav')
                print(des_path)
                des_dir=os.path.dirname(des_path)
                if not os.path.isdir(des_dir):
                    os.makedirs(des_dir)
                song.export(des_path,'wav')

def singer_name_to_start(artist_dir):
    for root , dirs, names in os.walk(artist_dir):
        for name in names:
            file_path=os.path.join(root,name)
            if '.wav' in file_path:
                print(file_path)
                file_path_list=file_path.split(os.sep)
                des_path=os.path.join(root,file_path_list[1]+'.'+name)
                print(des_path)
                os.rename(file_path,des_path)


def name_in_10(artist_dir):
    for root , dirs, names in os.walk(artist_dir):
        num=1
        for name in names:
            file_path=os.path.join(root,name)
            if '.wav' in file_path:
                print(file_path)
                file_path_list=file_path.split(os.sep)

                des_path=os.path.join(root,file_path_list[1]+'.%03d.wav'%num)
                num+=1

                print(des_path)
                os.rename(file_path,des_path)

if __name__ == '__main__':
    name_in_10('../../artist20')